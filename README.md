### Building

This `index.html` web page makes an HTTP GET request to the [weather-api](https://gitlab.com/cpit490/weather-api).

You will need to dockerize this html page using the `httpd` Docker image.
- Run the [weather-api](https://gitlab.com/cpit490/weather-api).
- Replace the fetch host/IP address and port number to match the one that the `weather-api` is running on. You will need to [edit this line](https://gitlab.com/cpit490/weather-web/-/blob/main/index.html#L26)
- create a `Dockerfile` as follows:
  - use the `httpd:2-alpine` as the base image
  - Copy the `index.html` page into `/usr/local/apache2/htdocs/`
- build the container image and run the Docker container:

```bash
$ docker build -t my-web-app .
$ docker run -it --name my-web-app-container -p 8080:80 my-web-app
```
